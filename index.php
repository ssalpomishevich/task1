<?php

include "functions.php";

$filename = "numbers.txt"; // файл с набором чисел
$numberStr = getTextFromFile($filename); // строка из файла
$oper = "/"; // тип операции, принимает (* / + -)

// Название файлов для записи результатов работы
$positiveValuesFile = "positive_values.txt";
$negativeValuesFile = "negative_values.txt";

$numbers = explode("\n", $numberStr); // делим каждую строку в отдельный массив

foreach ($numbers as $number) {
    if (preg_match('/^(-?\d+)\s(-?\d+)$/', $number, $matches)) {

        $result = calculate($matches[1], $matches[2], $oper);
        $text = sprintf("%s %s %s = %s \n", $matches[1], $oper, $matches[2], $result);

        if ($result >= 0) {
            writeToFile($positiveValuesFile, $text);
        } else {
            writeToFile($negativeValuesFile, $text);
        }

    }
}

writeToMultipleFile(
    implode(",", array($positiveValuesFile, $negativeValuesFile)),
    "===========================\n"
);

