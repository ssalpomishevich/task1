<?php

function calculate($a, $b, $oper)
{
    $res = false;

    switch ($oper) {
        case "*":
            $res = $a * $b;
            break;
        case "/":
            $res = $a / $b;
            break;
        case "+":
            $res = $a + $b;
            break;
        case "-":
            $res = $a - $b;
            break;
    }

    return $res;
}

// Читает текст из файла
function getTextFromFile($file)
{
    $result = "";

    $file = fopen($file, "r") or die("Unable to open file!");

    while (!feof($file)) {
        $result .= fgetc($file);
    }

    fclose($file);

    return $result;
}

// Записывает текст в файл
function writeToFile($filename, $text, $mode = "a")
{
    $fh = fopen($filename, $mode);
    fwrite($fh, $text);
    fclose($fh);
}

// записывает текст сразу в несколько файлов
function writeToMultipleFile($files, $text, $mode = "a")
{
    $files = explode(",", $files);

    foreach ($files as $file) {
        writeToFile($file, $text, $mode);
    }
}